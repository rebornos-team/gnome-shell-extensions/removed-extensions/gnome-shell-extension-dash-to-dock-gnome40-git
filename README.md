# gnome-shell-extension-dash-to-dock-gnome40-git

Move the dash out of the overview transforming it in a dock (with GNOME 40 patches)

https://micheleg.github.io/dash-to-dock/

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-shell-extension-dash-to-dock-gnome40-git.git
```

